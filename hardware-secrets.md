---
title: OpenPGP Hardware-Backed Secret Keys
docname: draft-dkg-openpgp-hardware-secrets-01
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft
submissionType: IETF

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: D. K. Gillmor
    name: Daniel Kahn Gillmor
    org: American Civil Liberties Union
    street: 125 Broad St.
    city: New York, NY
    code: 10004
    country: USA
    abbrev: ACLU
    email: dkg@fifthhorseman.net
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/dkg/openpgp-hardware-secrets/"
  latest: "https://dkg.gitlab.io/openpgp-hardware-secrets/"
informative:
 OPENPGP-SMARTCARD:
   target: https://gnupg.org/ftp/specs/OpenPGP-smart-card-application-3.4.1.pdf
   title: Functional Specification of the OpenPGP application on ISO Smart Card Operating Systems, Version 3.4.1
   date: 2020-03-18
   author:
    -
     name: Achim Pietig
 GNUPG-SECRET-STUB:
   target: "https://dev.gnupg.org/source/gnupg/browse/master/doc/DETAILS;gnupg-2.4.3$1511"
   title: GNU Extensions to the S2K algorithm
   date: 2023-07-04
   author:
    -
     name: Werner Koch
     org: g10 Code
 TPM:
   target: "https://trustedcomputinggroup.org/resource/tpm-library-specification/"
   title: "Trusted Platform Module Library Specification, Family “2.0”, Level 00, Revision 01.59"
   date: November 2019
   author:
    -
     org: Trusted Computing Group
 SMART-CARD-FAULTS:
   target: "http://hdl.handle.net/2117/99293"
   title: Smart Card Fault Injections with High Temperatures
   date: 2016-11-15
   author:
    -
     name: Pedro Maat C. Massolino
    -
     name: Baris Ege
    -
     name: Lejla Batina
   
--- abstract

This document defines a standard wire format for indicating that the secret component of an OpenPGP asymmetric key is stored on a hardware device.

--- middle

# Introduction

Some OpenPGP secret key material is held by hardware devices that permit the user to operate the secret key without divulging it explicitly.
For example, the {{OPENPGP-SMARTCARD}} specification is intended specifically for this use.
It may also possible for OpenPGP implementations to use hardware-backed secrets via standard platform library interfaces like {{TPM}}.

An OpenPGP Secret Key Packet (see {{Section 5.5.3 of !I-D.ietf-openpgp-crypto-refresh}}) is typically used as part of a Transferable Secret Key ({{Section 10.2 of !I-D.ietf-openpgp-crypto-refresh}}) for interoperability between OpenPGP implementations.
An implementation that uses a hardware-backed secret key needs a standardized way to indicate to another implementation specific secret key material has been delegated to some hardware device.

This document defines a simple mechanism for indicating that a secret key has been delegated to a hardware device by allocating a codepoint in the "Secret Key Encryption (S2K Usage Octet)" registry (see {{Section 3.7.2.1 of I-D.ietf-openpgp-crypto-refresh}}).

This document makes no attempt to specify how an OpenPGP implementation discovers, enumerates, or operates hardware, other than to recommend that the hardware should be identifiable by the secret key's corresponding public key material.

## Requirements Language

{::boilerplate bcp14-tagged}

## Terminology

"Secret key" refers to a single cryptographic object, for example the "56 octets of the native secret key" of X448, as described in {{Section 5.5.5.8 of I-D.ietf-openpgp-crypto-refresh}}.

"Public key" likewise refers to a single cryptographic object, for example the "56 octets of the native public key" of X448, as above.

"OpenPGP certificate" or just "certificate" refers to an OpenPGP Transferable Public Key (see {{Section 10.1 of I-D.ietf-openpgp-crypto-refresh}}).

"Hardware" refers to any cryptographic device or subsystem capable of performing an asymmetric secret key operation using an embedded secret key without divulging the secret to the user.
For discoverability, the hardware is also expected to be able to produce the public key corresponding to the embedded secret key.

While this document talks about "hardware" in the abstract as referring to a cryptographic device embedding a single secret key, most actual hardware devices will embed and enable the use of multiple secret keys (see {{multiple-key-hardware}}).

This document uses the term "authorization" to mean any step, such as providing a PIN, password, proof of biometric identity, button-pushing, etc, that the hardware may require for an action.

# Hardware-backed Secret Key Material {#spec}

An OpenPGP Secret Key packet ({{Section 5.5.3 of I-D.ietf-openpgp-crypto-refresh}}) indicates that the secret key material is stored in cryptographic hardware that is identifiable by public key parameters in the following way.

The S2K usage octet is set to TBD (252?), known in shorthand as `HardwareBacked`.
A producing implementation MUST NOT include any trailing data in the rest of such a Secret Key packet.
A consuming implementation MUST ignore any trailing data in such a Secret Key packet.

# Security Considerations

Hardware-backed secret keys promise several distinct security advantages to the user:

- The secret key cannot be extracted from the device, so "kleptography" (the stealing of secret key material) is harder to perform.

- Some hardware can be moved between machines, enabling secret key portability without expanding the kleptographic attack surface.

- Some hardware devices offer auditability controls in the form of rate-limiting, user-visible authorization steps (e.g., button-presses or biometric sensors), or tamper-resistant usage counters.
  Malicious use of a secret key on such a device should be harder, or at least more evident.

- Some hardware security devices can attest that key material has been generated on-card, thereby signaling that - barring a successful attack on the hardware - no other copy of the private key material exists.
  Such mechanisms signal that the key holder did not have a chance to mishandle (e.g.: accidentally disclose) the private key material.

However, none of these purported advantages are without caveats.

The hardware itself might actually not resist secret key exfiltration as expected.
For example, isolated hardware devices are sometimes easier to attack physically, via temperature or voltage fluctuations (see {{?VOLTAGE-GLITCHING=DOI.10.48550/arXiv.2108.06131}} and {{SMART-CARD-FAULTS}}).

In some cases, dedicated cryptographic hardware that generates a secret key internally may have significant flaws (see {{?ROCA=DOI.10.1145/3133956.3133969}}).

Furthermore, the most sensitive material in the case of decryption is often the cleartext itself, not the secret key material.
If the host computer itself is potentially compromised, then kleptographic exfiltration of the secret key material itself is only a small risk.
For example, the OpenPGP symmetric session key itself could be exfiltrated, permitting access to the cleartext to anyone without access to the secret key material.

Portability brings with it other risks, including the possibility of abuse by the host software on any of the devices to which the hardware is connected.

Rate-limiting, user-visible authorization steps, and any other form of auditability also suffer from risks related to compromised host operating systems.
Few hardware devices are capable of revealing to the user what operations specifically were performed by the device, so even if the user deliberately uses the device to, say, sign an object, the user depends on the host software to feed the correct object to the device's signing capability.

# Usability Considerations

Hardware-backed secret keys present specific usability challenges for integration with OpenPGP.

## Some Hardware Might Be Unavailable To Some Implementations

This specification gives no hints about how to find the hardware device, and presumes that an implementation will be able to probe available hardware to associate it with the corresponding public key material.
In particular, there is no attempt to identify specific hardware or "slots" using identifiers like PKCS #11 URIs ({{?RFC7512}}) or smartcard serial numbers (see {{historical-notes}}).
This minimalism is deliberate, as it's possible for the same key material to be available on multiple hardware devices, or for a device to be located on one platform with a particular hardware identifier, while on another platform it uses a different hardware identifier.

Not every OpenPGP implementation will be able to talk to every possible hardware device.
If an OpenPGP implementation encounters a hardware-backed secret key as indicated with this mechanism, but cannot identify any attached hardware that lists the corresponding secret key material, it should warn the user that the specific key claims to be hardware-backed but the corresponding hardware cannot be found.
It may also want to inform the user what categories of hardware devices it is capable of probing, for debugging purposes.

## Hardware Should Support Multiple Secret Keys {#multiple-key-hardware}

Most reasonable OpenPGP configurations require the use of multiple secret keys by a single operator.
For example, the user may use one secret key for signing, and another secret key for decryption, and the corresponding public keys of both are contained in the same OpenPGP certificate.

Reasonable hardware SHOULD support embedding and identifying more than one secret key, so that a typical OpenPGP user can rely on a single device for hardware backing.

## Authorization Challenges

Cryptographic hardware can be difficult to use if frequent authorization is required, particularly in circumstances like reading messages in a busy e-mail inbox.
This hardware MAY require authorization for each use of the secret key material as a security measure, but considerations should be made for caching authorization 

If the cryptographic hardware requires authorization for listing the corresponding public key material, it becomes even more difficult to use the device in regular operation.
Hardware SHOULD NOT require authorization for the action of producing the corresponding public key.

If a user has two attached pieces of hardware that both hold the same secret key, and one requires authorization while the other does not, it is reasonable for an implementation to try the one that doesn't require authorization first.
Some cryptographic hardware is designed to lock the device on repeated authorization failures (e.g. 3 bad PIN entries locks the device), so this approach reduces the risk of accidental lockout.

## Latency and Error Handling

While hardware-backed secret key operations can be significantly slower than modern computers, and physical affordances like button-presses or NFC tapping can themselves incur delay, an implementation using a hardware-backed secret key should remain responsive to the user.
It should indicate when some interaction with the hardware may be required, and it should use a sensible timeout if the hardware device appears to be unresponsive.

A reasonable implementation should surface actionable errors or warnings from the hardware to the user where possible.

# IANA Considerations

This document asks IANA to make two changes in the "OpenPGP" protocol group.

Add the following row in the "OpenPGP Secret Key Encrpytion (S2K Usage Octet)" registry:

{: title="Row to add to OpenPGP Secret Key Encrpytion (S2K Usage Octet) registry"}
S2K usage octet | Shorthand | Encryption parameter fields | Encryption | Generate?
-----|----|---|---|---
TBD (252?) | HardwareBacked | none | no data, see {{spec}} of RFC XXX (this document) | Yes

Modify this row of the "OpenPGP Symmetric Key Algorithms" registry:

{: title="Row to modify in OpenPGP Symmetric Key Algorithms registry"}
ID | Algorithm
--:|---
253, 254, and 255 | Reserved to avoid collision with Secret Key Encryption

to include TBD (252?) in this reserved codepoint sequence, resulting in the following entry:

{: title="Modified row in OpenPGP Symmetric Key Algorithms registry"}
ID | Algorithm
--:|---
TBD (252?), 253, 254, and 255 | Reserved to avoid collision with Secret Key Encryption

--- back

# Historical notes

Some OpenPGP implementations make use of private codepoint ranges in the OpenPGP specification within an OpenPGP Transferable Secret Key to indicate that the secret key can be found on a smartcard.

For example, GnuPG uses the private/experimental codepoint 101 in the S2K Specifier registry, along with an embedded trailer with an additional codepoint, plus the serial number of the smartcard (see {{GNUPG-SECRET-STUB}}).

However, recent versions of that implementation ignore the embedded serial number in favor of scanning available devices for a match of the key material, since some people have multiple cards with the same secret key.


# Test vectrors

## Example Transferable Secret Key

The OpenPGP Transferable Secret Key used for this example. It includes (unencrypted) private key material for both its primary key and one subkey:

```
-----BEGIN PGP PRIVATE KEY BLOCK-----

xVgEZgWtcxYJKwYBBAHaRw8BAQdAlLK6UPQsVHR2ETk1SwVIG3tBmpiEtikYYlCy
1TIiqzYAAQCwm/O5cWsztxbUcwOHycBwszHpD4Oa+fK8XJDxLWH7dRIZzR08aGFy
ZHdhcmUtc2VjcmV0QGV4YW1wbGUub3JnPsKNBBAWCAA1AhkBBQJmBa1zAhsDCAsJ
CAcKDQwLBRUKCQgLAhYCFiEEXlP8Tur0WZR+f0I33/i9Uh4OHEkACgkQ3/i9Uh4O
HEnryAD8CzH2ajJvASp46ApfI4pLPY57rjBX++d/2FQPRyqGHJUA/RLsNNgxiFYm
K5cjtQe2/DgzWQ7R6PxPC6oa3XM7xPcCx10EZgWtcxIKKwYBBAGXVQEFAQEHQE1Y
XOKeaklwG01Yab4xopP9wbu1E+pCrP1xQpiFZW5KAwEIBwAA/12uOubAQ5nhf1UF
a51SQwFLpggB/Spn29qDnSQXOTzIDvPCeAQYFggAIAUCZgWtcwIbDBYhBF5T/E7q
9FmUfn9CN9/4vVIeDhxJAAoJEN/4vVIeDhxJVTgA/1WaFrKdP3AgL0Ffdooc5XXb
jQsj0uHo6FZSHRI4pchMAQCyJnKQ3RvW/0gm41JCqImyg2fxWG4hY0N5Q7Rc6Pyz
DQ==
=lYbx
-----END PGP PRIVATE KEY BLOCK-----
```

## As a Hardware-Backed Secret Key

The same OpenPGP Transferable Secret Key with the S2K Usage Octet set to 252? (HardwareBacked) for both the Primary Key Packet and the Subkey Packet. This format omits all data following the S2K Usage Octet:

```
-----BEGIN PGP PRIVATE KEY BLOCK-----

xTQEZgWtcxYJKwYBBAHaRw8BAQdAlLK6UPQsVHR2ETk1SwVIG3tBmpiEtikYYlCy
1TIiqzb8zR08aGFyZHdhcmUtc2VjcmV0QGV4YW1wbGUub3JnPsKNBBAWCAA1AhkB
BQJmBa1zAhsDCAsJCAcKDQwLBRUKCQgLAhYCFiEEXlP8Tur0WZR+f0I33/i9Uh4O
HEkACgkQ3/i9Uh4OHEnryAD8CzH2ajJvASp46ApfI4pLPY57rjBX++d/2FQPRyqG
HJUA/RLsNNgxiFYmK5cjtQe2/DgzWQ7R6PxPC6oa3XM7xPcCxzkEZgWtcxIKKwYB
BAGXVQEFAQEHQE1YXOKeaklwG01Yab4xopP9wbu1E+pCrP1xQpiFZW5KAwEIB/zC
eAQYFggAIAUCZgWtcwIbDBYhBF5T/E7q9FmUfn9CN9/4vVIeDhxJAAoJEN/4vVIe
DhxJVTgA/1WaFrKdP3AgL0Ffdooc5XXbjQsj0uHo6FZSHRI4pchMAQCyJnKQ3RvW
/0gm41JCqImyg2fxWG4hY0N5Q7Rc6PyzDQ==
=3w/O
-----END PGP PRIVATE KEY BLOCK-----
```

The (primary) Secret-Key Packet of this key looks as follows, in this format:

```
00000000  c5                                                 CTB
00000001     34                                              length
00000002        04                                           version
00000003           66 05 ad 73                               creation_time
00000007                       16                            pk_algo
00000008                           09                        curve_len
00000009                              2b 06 01 04 01 da 47   curve
00000010  0f 01
00000012        01 07                                        eddsa_public_len
00000014              40 94 b2 ba  50 f4 2c 54 74 76 11 39   eddsa_public
00000020  35 4b 05 48 1b 7b 41 9a  98 84 b6 29 18 62 50 b2
00000030  d5 32 22 ab 36
00000035                 fc                                  s2k_usage
```

# Acknowledgements

This work depends on a history of significant work with hardware-backed OpenPGP secret key material, including useful implementations and guidance from many people, including:

- NIIBE Yutaka
- Achim Pietig
- Werner Koch
- Heiko Schäfer

The people acknowledeged in this section are not responsible for any proposals, errors, or omissions in this document.

# Document History

